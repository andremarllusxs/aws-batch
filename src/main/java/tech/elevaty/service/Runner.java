package tech.elevaty.service;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import tech.elevaty.util.FileUtil;

import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

@Component
public class Runner implements ApplicationRunner {

    private final ApplicationContext context;

    public Runner(ApplicationContext context) {
        this.context = context;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("Hello Word");

        List<String> nome = args.getOptionValues("nome");

        if (nome != null && !nome.isEmpty())
            System.out.println(nome.get(0));

        FileUtil.unzip("./file.zip", "./");

        int i = 0;
        try (SeekableByteChannel seekableByteChannel = Files.newByteChannel(Paths.get("./file.txt"), StandardOpenOption.READ)) {
            ByteBuffer byteBuffer = ByteBuffer.allocate(1000);
            StringBuilder lineBuilder = new StringBuilder();
            while (seekableByteChannel.read(byteBuffer) > 0) {
                byteBuffer.flip();
                while (byteBuffer.hasRemaining()) {
                    char currentChar = (char) byteBuffer.get();
                    if (isBreakLine(currentChar) ) {
                        if(!lineBuilder.isEmpty()){
                            i++;
                            if(i%1000==0){
                                System.out.println(i);
                                System.out.println(lineBuilder);
                                lineBuilder.setLength(0);
                                lineBuilder= new StringBuilder();
                            }

                        }
                    }else{
                        lineBuilder.append(currentChar);
                        lineBuilder.setLength(0);
                    }

                }
                byteBuffer.clear();
            }
        }
        SpringApplication.exit(context, () -> 0);
    }

    protected boolean isBreakLine(char theChar){
        return ( theChar == BREAK_LINE_n || theChar == BREAK_LINE_r );
    }

    protected static final char BREAK_LINE_n = '\r';

    protected static final char BREAK_LINE_r = '\n';
}
