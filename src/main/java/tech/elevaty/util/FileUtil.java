package tech.elevaty.util;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class FileUtil {

    public static List<String> inputStreamToList(InputStream inputStream) {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.ISO_8859_1));
            List<String> collect = br.lines().collect(Collectors.toList());
            br.close();
            return collect;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static List<String> pathToList(Path path) {
        try {
            return Files.readAllLines(path, StandardCharsets.ISO_8859_1);
        }catch (IOException e){
            throw new RuntimeException(e);
        }
    }

    public static Path unzip(String fileZip, String unzipPath){
        try (ZipInputStream zipInputStream = new ZipInputStream(Files.newInputStream(Paths.get(fileZip)))) {
            Files.createDirectories(Paths.get(unzipPath));

            ZipEntry entry = zipInputStream.getNextEntry();

            assert entry != null;
            Path pathEnd = Paths.get(unzipPath, entry.getName());

            Files.createDirectories(pathEnd.getParent());

            Files.copy(zipInputStream, pathEnd, StandardCopyOption.REPLACE_EXISTING);

            zipInputStream.closeEntry();

            return Path.of(unzipPath + File.separator + entry.getName());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getFileName(String pathFile) {
        Path path = FileSystems.getDefault().getPath(pathFile);
        return path.getFileName().toString();
    }

    public static void deleteFiles(Path ... invoiceFilePathZip) {
        for(Path path : invoiceFilePathZip) {
            try {
                Files.delete(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
