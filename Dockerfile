FROM azul/zulu-openjdk-alpine:21

RUN apk add --no-cache tzdata

ENV TZ America/Sao_Paulo

COPY target/*.jar app.jar
COPY file.zip file.zip

CMD ["java", "-jar", "app.jar"]